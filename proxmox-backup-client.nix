#
# Copyright © 2022 Emil Johansen
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#
# Example use:
#
#  services.proxmox.backup.client = {
#    enable = true;
#    host = "backup-server-address";
#    store = "backup-store-name";
#    user = "user-name";
#    token = {
#      name = "token-name";
#      secret = "token-secret";
#    };
#    archives = {
#      root = "/";
#      home = "/home";
#      log = "/var/log";
#    };
#  };
#
# Terminal tools:
#   backup-init
#   backup-run
#   backup-browse
#
#
{ lib, pkgs, config, ... }:
let
  cfg = config.services.proxmox.backup.client;
  defaults = {
    keyfile = "/etc/nixos/src/pbs.key";
    mount = "/mnt/backup";
  };

  backup = {
    keyfile = cfg.encryption.keyfile;
    mount = defaults.mount;
    repository = "${cfg.user}@pbs!${cfg.token.name}@${cfg.host}:${cfg.store}";
    secret = cfg.token.secret;
    scripts = {
      init = pkgs.writeScriptBin "backup-init" ''
        if [ $UID -ne 0 ]; then
          sudo $0
          exit
        fi

        if [ -e "${backup.keyfile}" ]; then
          echo "Keyfile already exists at ${backup.keyfile}"
        else
          echo "Generating new keyfile"
          ${pkgs.proxmox-backup-client}/bin/proxmox-backup-client key create "${backup.keyfile}" --kdf none
        fi
        
        ${pkgs.proxmox-backup-client}/bin/proxmox-backup-client key paperkey "${backup.keyfile}" --output-format text
      '';
      run = pkgs.writeScriptBin "backup-run" (''
        if [ $UID -ne 0 ]; then
          sudo $0
          exit
        fi

        PBS_REPOSITORY="${backup.repository}"\
          PBS_PASSWORD="${backup.secret}"\
          ${pkgs.proxmox-backup-client}/bin/proxmox-backup-client backup'' +
          (lib.concatMapStrings (argument: " " + argument)
            (lib.mapAttrsToList (name: path: "${name}.pxar:${path}") cfg.archives
          )) + " " +
          ''--keyfile "${backup.keyfile}"
      '');
      browse = pkgs.writeScriptBin "backup-browse" ''
        if [ $UID -ne 0 ]; then
          sudo $0 $1 $2
          exit
        fi

        if [[ $# -ne 2 ]]; then
          PBS_REPOSITORY="${backup.repository}"\
            PBS_PASSWORD="${backup.secret}"\
            ${pkgs.proxmox-backup-client}/bin/proxmox-backup-client snapshot list
          echo "Mount a shapshot: backup-browse <snapsnot> <file.pxar>"
          exit
        fi

        snapshot=$1
        archive=$2

        umount "${backup.mount}" 2>&1 1>/dev/null || true
        mkdir -p "${backup.mount}"
        PBS_REPOSITORY="${backup.repository}"\
          PBS_PASSWORD="${backup.secret}"\
          ${pkgs.proxmox-backup-client}/bin/proxmox-backup-client mount "$snapshot" "$archive" "${backup.mount}" --keyfile "${backup.keyfile}"

        echo
        echo "$archive of $snapshot mounted at ${backup.mount}"

        if [ -z "$DISPLAY" ]; then
          ls -al "${backup.mount}/"
          exit
        fi

        echo "Opening ${backup.mount} in default file browser"

        ${pkgs.mimeo}/bin/mimeo "${backup.mount}" &>/dev/null
      '';
    };
  };
in
with lib;
{
  options.services.proxmox.backup.client = {
    enable = mkEnableOption "Proxbox backup client service";
    host = mkOption {
      type = types.str;
      description = "Proxmox Backup Server host";
    };
    store = mkOption {
      type = types.str;
      description = "Name of the target store on the specified host";
    };
    user = mkOption {
      type = types.str;
      description = "Name of the user used for authentication";
    };
    token = {
      name = mkOption {
        type = types.str;
        description = "Name of the token used for authentication";
      };
      secret = mkOption {
        type = types.str;
        description = "Secret of the token used for authentication";
      };
    };
    # TODO: Add alternate option for directly sourced key value encryption.key
    encryption.keyfile = mkOption {
      type = types.str;
      description = "Path to where the backup encryption keyfile is/should be stored";
      default = defaults.keyfile;
    };
    archives = mkOption {
      type = with types; attrsOf str;
      description = "Pairs of `archive-name = 'local path'` specifying what to back up";
      default = { root = "/"; };
    };
  };

  config = mkIf cfg.enable
  {
    environment = {
      systemPackages = with backup.scripts; [
        init
        run
        browse
      ];
    };

    systemd.services.backup = {
      serviceConfig = {
        Type = "simple";
        User = "root";
        ExecStart = "/run/current-system/sw/bin/sh ${backup.scripts.run}/bin/backup-run";
        Restart = "always";
        RestartSec = 60*60;
      };
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      startLimitIntervalSec = 0;
    };
  };
}
